var store = [{
        "title": "Recursion in Clojure",
        "excerpt":"Just want to make a note so that I don’t forget. I am reading Clojure to build very complex, concurrent systems that deal with lot of data. Using OOPS for that is a pain, it collapses when we hit about 0.2 million lines of code. I had done recursion in...","categories": [],
        "tags": [],
        "url": "/2022/04/21/recursion-in-clojure.html",
        "teaser": null
      },{
        "title": "Why this blog?",
        "excerpt":"I am learning about Clojure a lot, I will be writing why I am choosing Clojure now in my career. This blog is mainly for my self documentation. Here I will document my Clojure successes, pitfalls, work arounds, tutorials etc. I hope this would grow into a blog that would...","categories": [],
        "tags": [],
        "url": "/2022/05/21/why-this-blog.html",
        "teaser": null
      },{
        "title": "Why Clojure?",
        "excerpt":"In this blog I would like to tell why I am choosing Clojure. I hope you like it. Bigger and bigger projects As a software architect, I am looking at bigger and bigger projects. Some projects are about 0.2+ million lines. It was written in Ruby on Rails, and had...","categories": [],
        "tags": [],
        "url": "/2022/05/22/why-clojure.html",
        "teaser": null
      },{
        "title": "The books I am reading to learn Clojure",
        "excerpt":"There are many Clojure books, it would be confusing to choose one to learn. Here I would like blog what books are the best to start learning Clojure. Getting Clojure The first book that worked for me is Getting Clojure, it makes it really simple for one to start with...","categories": [],
        "tags": [],
        "url": "/2022/05/27/the-books-i-am-reading-to-learn-clojure.html",
        "teaser": null
      },{
        "title": "Night Light IDE seems to be excellent for Clojure learning",
        "excerpt":"I happened to download Night Light IDE https://sekao.net/nightlight/ for Clojure and I was blown away! The first thing was how minimal it was. It gave the right tools to start and continue with the project, the second was how easy it was to launch REPL in it. Look at the...","categories": [],
        "tags": [],
        "url": "/2022/05/29/night-light-ide-seems-to-be-excellent-for-clojure-learning.html",
        "teaser": null
      },{
        "title": "I kind of understand Clojure macros",
        "excerpt":"Looks like I have understood about macros in Clojure (a bit). So whats a macro? Lets say you want to add 2 and 3 and you will write it like this: (+ 2 3) Where + is a function and 2 and 3 are arguments. But let’s say that I...","categories": [],
        "tags": [],
        "url": "/2022/06/29/i-kind-of-understand-clojure-macros.html",
        "teaser": null
      },{
        "title": "Clojure Workshop and Calva",
        "excerpt":"Clojure Workshop I have started reading this book Clojure Workshop This book promises to teach Clojure from start and claims that at the end of this book we would have developed a web app. Being a Ruby on Rails developer, I am exited by this promise. I am in its...","categories": [],
        "tags": [],
        "url": "/2022/07/01/clojure-workshop-and-calva.html",
        "teaser": null
      },{
        "title": "Printing n numbers in Clojure",
        "excerpt":"Somewhere I saw a question where one needs to print from 1 to a number in Clojure, and this is a way it could be done: ;; print-n-numbers.clj (require '[clojure.string :as string]) (let [number 10] ; setting variable number (println ; let's print the joined sequence (string/join \"\\n\" ; joining...","categories": [],
        "tags": [],
        "url": "/2022/07/02/printing-n-numbers.html",
        "teaser": null
      },{
        "title": "Clojure REPL in VSCodium",
        "excerpt":"             Code   celcius-to-fahrenheit.clj   (defn celcius-to-fahrenheit [celcius]   (+ (* (/ 9 5) celcius) 32))   loadfile.clj   (load-file \"celcius-to-fahrenheit.clj\")  (celcius-to-fahrenheit 100)   Notes      Installing Clojure   Leningen   VSCodium   Calva   Joyride  ","categories": [],
        "tags": [],
        "url": "/2022/07/06/clojure-repl-in-vscodium.html",
        "teaser": null
      },{
        "title": "Started writing libre and gratis Clojure book",
        "excerpt":"If you all know, I am moving to Clojure from Ruby on Rails because I think I need not face JavaScript, and because people talk a lot about Clojure and its greatness. I think I have enough data to start writing about introductory Clojure book. But I am not sure...","categories": [],
        "tags": [],
        "url": "/2022/07/09/started-writing-libre-and-gratis-clojure-book.html",
        "teaser": null
      },{
        "title": "ds4clj - Data Science for Clojure",
        "excerpt":"I started Clojure to avoid JavaScript while developing web apps. And when you use OOP, when the code size increases to a decimillion lines of code, one can see the strain of maintaining it. I practiced my Data Science skills with Julia, an excellent language developed for scientific computing. But...","categories": [],
        "tags": [],
        "url": "/2022/07/29/ds4clj-data-science-for-clojure.html",
        "teaser": null
      },{
        "title": "load-file fails to work in REPL",
        "excerpt":"I had a strange problem with load-file yesterday, for some reason it did not work in REPL. This was the code I had: ;; lcm.clj (load-file \"gcd_function.clj\") (defn lcm [first-number second-number] (/ (* first-number second-number) (gcd first-number second-number))) (println (lcm 12 14)) This worked when I did this: $ clj...","categories": [],
        "tags": [],
        "url": "/2022/09/04/load-file-fails-to-work-in-repl.html",
        "teaser": null
      },{
        "title": "My Clojure book gradually gets into shape",
        "excerpt":"I am happy to say that my Clojure book is gradually getting into shape. When ever I get time, I am adding few examples into it. Today I realized that I had added enough examples into Functions to make it almost complete. Functions are at the heart of Clojure, and...","categories": [],
        "tags": [],
        "url": "/2022/09/07/my-clojure-book-gradually-gets-into-shape.html",
        "teaser": null
      },{
        "title": "My next clojure steps",
        "excerpt":"I am kind of finishing Getting Clojure book. I don’t understand namespaces in Clojure well. Apart from that all is okay. I am planning for my next Clojure steps, they are as follows: Tic-tac-toe First I want to write a tic-tac-toe player in Clojure. It will be a console based...","categories": [],
        "tags": [],
        "url": "/2022/09/26/my-next-clojure-steps.html",
        "teaser": null
      },{
        "title": "Writing a prime number function in Clojure",
        "excerpt":"             Notes   (defn prime? [number]  (every? false?          (map #(= (mod number %) 0) (range 2 number))))  (prime? 21) ; false (prime? 11) ; true  ","categories": [],
        "tags": [],
        "url": "/2022/10/02/writing-a-prime-number-function-in-clojure.html",
        "teaser": null
      },{
        "title": "Finding primes from 1 to 100 in Clojure",
        "excerpt":"             Notes   ;; prime.clj  (defn prime? [number]  (every? false?          (map #(= (mod number %) 0) (range 2 number))))   ;; primes_till_100.clj  (load-file \"prime.clj\")  (defn return-number-if-prime [number]   (when (prime? number) number))  (remove nil?  (map #(return-number-if-prime %) (range 1 101)))      Writing a prime number function in Clojure  ","categories": [],
        "tags": [],
        "url": "/2022/10/02/finding-primes-from-1-to-100-in-clojure.html",
        "teaser": null
      },{
        "title": "Foo bar in Clojure",
        "excerpt":"             Notes   ;; foo_bar.clj  (defn foo [number]   (if (= (mod number 3) 0)     \"foo\"     \"\"))  (defn bar [number]   (if (= (mod number 5) 0)     \"bar\"     \"\"))  (map  #(println % \"-\" (foo %) (bar %))  (range 1 31))  ","categories": [],
        "tags": [],
        "url": "/2022/10/02/foo-bar-in-clojure.html",
        "teaser": null
      },{
        "title": "Making foo bar more readable in Clojure",
        "excerpt":"Notes ;; foo_bar_redable.clj (defn divisible? \" if x is divisble by y, it returns true \" [x y] (when (= (mod x y) 0) true)) (defn foo [number] (if (divisible? number 3) \"foo\" \"\")) (defn bar [number] (if (divisible? number 5) \"bar\" \"\")) (defn foobar-string [number] (str number \" -...","categories": [],
        "tags": [],
        "url": "/2022/10/02/making-foo-bar-more-readable-in-clojure.html",
        "teaser": null
      },{
        "title": "Letter pyramid in Clojure",
        "excerpt":"             Notes   ;; letter_pyramid.clj  (def chars-list   (map #(char (+ % 65)) (range 26)))  (defn line [number]   (clojure.string/join \"\" (take number chars-list)))  (println (clojure.string/join \"\\n\" (map line (range 1 27))))      A to Z Pyramid in Ruby  ","categories": [],
        "tags": [],
        "url": "/2022/10/03/letter-pyramid-in-clojure.html",
        "teaser": null
      },{
        "title": "Centering text in Clojure",
        "excerpt":"Notes ;; centering_text.clj (def page-size 20) (defn padding [page-size line-size] (/ (- page-size line-size) 2)) (defn spaces [length] (let [infinite-space (repeat \" \")] (clojure.string/join \"\" (take length infinite-space)))) (defn centered-line [page-size line] (let [line-size (count line) pad-size (padding page-size line-size)] (str (spaces pad-size ) line))) (println (centered-line page-size \"Hello\")) (println...","categories": [],
        "tags": [],
        "url": "/2022/10/03/centering-text-in-clojure.html",
        "teaser": null
      },{
        "title": "Fibonacci in Clojure",
        "excerpt":"Notes number &lt;- index 0 &lt;- 1 1 &lt;- 2 1 &lt;- 3 2 &lt;- 4 3 &lt;- 5 5 &lt;- 6 8 &lt;- 7 13 &lt;- 8 21 &lt;- 9 (fib 5) / \\ (fib 4) (fib 3) / \\ / \\ (fib 3) (fib 2) (fib 2) (fib...","categories": [],
        "tags": [],
        "url": "/2022/10/04/fibonacci-in-clojure.html",
        "teaser": null
      },{
        "title": "Fibonacci properly memoized in Clojure",
        "excerpt":"             Notes   ;; memonize_fib_right_way.clj  (def fib (memoize           (fn [index]             (case index               1 0               2 1               (+                (fib (dec index))                (fib (- index 2))))))) (fib 52)      Reddit Comments  ","categories": [],
        "tags": [],
        "url": "/2022/10/05/fibonacci-properly-memoized-in-clojure.html",
        "teaser": null
      },{
        "title": "Debugging in Clojure",
        "excerpt":"Notes VSCodium Calva For executing whole code block choose ALT or Option + ENTER For executing block in which cursor is in, choose CTRL + SHIFT + ENTER ;; debugging.clj (defn bu [string] (str \"bu\" \" \" string)) (defn fu [string] (str \"fu\" \" \" string)) (defn du [string] (str...","categories": [],
        "tags": [],
        "url": "/2022/10/06/debugging-in-clojure.html",
        "teaser": null
      },{
        "title": "Clojure is infectious, Mac is repulsive",
        "excerpt":"             Notes   People who you can follow to keep up your Clojure passion      @kiraemclean - A passionate Clojurist   @lambduhh - A fire dancer who is now a Clojure programmer   @borkdude - A passionate ClojureScript guy who was a singer   @daslu_ - A Data Scientist using Clojure  ","categories": [],
        "tags": [],
        "url": "/2022/10/08/clojure-is-infectious-mac-is-repulsive.html",
        "teaser": null
      },{
        "title": "Unsafe Python, safe Clojure",
        "excerpt":"Notes # dangerous_python.py def check_validity(emails): print(\"Checking validity\") emails.pop() print(\"Checked!!\") def send_notification(emails): for email in emails: print(f\"Notified {email}\") emails = [ \"a@a.com\", \"b@b.com\", \"c@c.com\", \"d@d.com\", \"e@e.com\", \"f@f.com\" ] check_validity(emails) send_notification(emails) ;; safe_clojure.clj (require '[clojure.string :as str]) (defn check_validity [emails] (println \"Checking validity\") (pop emails) (println \"Checked!!\")) (defn send_notification [email] (str \"Sending...","categories": [],
        "tags": [],
        "url": "/2022/10/09/unsafe-python-safe-clojure.html",
        "teaser": null
      },{
        "title": "Debugging Clojure with Leningen, Calva and nRepl",
        "excerpt":"             Notes      nRepl   Leningen   Calva   VSCodium   Debugging with Calva   Get the source code here https://gitlab.com/clojure-diary/code/dbg_and_break  ","categories": [],
        "tags": [],
        "url": "/2022/10/11/debugging-clojure-with-leningen-calva-and-nrepl.html",
        "teaser": null
      },{
        "title": "Arrow thing",
        "excerpt":"             Notes   ;; arrow_thing.clj  (defn bu [string]   (str \"bu\" \" \" string))  (defn fu [string]   (str \"fu\" \" \" string))  (defn du [string]   (str \"du\" \" \" string))  (bu  (fu   (du \"tu\")))  (-&gt;&gt; \"tu\" du fu bu)  (-&gt;&gt;  \"tu\"  du  fu  bu)  ","categories": [],
        "tags": [],
        "url": "/2022/10/12/arrow-thing.html",
        "teaser": null
      },{
        "title": "What really happens in memoize",
        "excerpt":"             ;; what_really_happens_in_momoize.clj  (def return-num (memoize           (fn [num]             (println \"in return(\" num \")\" )             num)))  (return-num 52) ;; excute more than once (return-num 42) ;; excute more than once  ","categories": [],
        "tags": [],
        "url": "/2022/10/18/what-really-happens-in-memoize.html",
        "teaser": null
      },{
        "title": "Type conversion and generating big Fibonacci numbers in Clojure",
        "excerpt":"Notes ;; bigger_fibonacci.clj (def fib (memoize (fn [index] (case index 1 0 2 1 (+ (bigint (fib (dec index))) (bigint (fib (- index 2)))))))) ;; memonize_fib_right_way.clj (def fib (memoize (fn [index] (case index 1 0 2 1 (+ (fib (dec index)) (fib (- index 2))))))) (fib 52) Clojure Data Structures...","categories": [],
        "tags": [],
        "url": "/2022/10/27/type-conversion-and-generating-big-fibonacci-numbers-in-clojure.html",
        "teaser": null
      },{
        "title": "Calculating compression gain - File compression in Clojure (1)",
        "excerpt":"Notes ;; text_analysis.clj (def file \"245-0.txt\") (defn words [text] (clojure.string/split text #\"\\s+\")) (defn remove-punctions [text] (clojure.string/replace text #\"\\W+\" \" \")) (defn words-in-file [file] (-&gt;&gt; file slurp remove-punctions words)) (defn word-frequencies-in-file [file] (-&gt;&gt; file words-in-file frequencies)) (defn compression-gain [word-frequncies] (map #(let [word (first %) word-length (count word) word-count (last %) gain...","categories": [],
        "tags": [],
        "url": "/2022/10/28/calculating-compression-gain-file-compression-in-clojure-1.html",
        "teaser": null
      },{
        "title": "for and doseq in Clojure",
        "excerpt":"Notes ;; for_and_doseq.clj (doseq [i (range 1 11)] (print i \", \")) (for [i (range 1 11)] (print i \", \")) (print 5) (def dice-faces (range 1 7)) (for [x dice-faces y dice-faces] [x y]) (doseq [x dice-faces y dice-faces] [x y]) (doseq [x dice-faces y dice-faces] (println [x y]))...","categories": [],
        "tags": [],
        "url": "/2022/11/03/for-and-doseq-in-clojure.html",
        "teaser": null
      },{
        "title": "Spine of my Clojure book",
        "excerpt":"I am happy how the spine of my Clojure book turned out to be. I feel functional programming is a great invention by humanity, and it should be compared with other great ones, so we decided to put the great inventions of humanity in the spine. Starting at the bottom...","categories": [],
        "tags": [],
        "url": "/2022/11/15/spine-of-my-clojure-book.html",
        "teaser": null
      },{
        "title": "Planning Beginning Clojure Web Development Book",
        "excerpt":"My Clojure book is going well, still very much in beta, but content for it is being gathered. Programming in Clojure is so good that I am adding code examples than writing content for the book :smile:. I am a professional Ruby on Rails developer, I make the most money...","categories": [],
        "tags": [],
        "url": "/2022/11/16/planning-beginning-clojure-web-development-book.html",
        "teaser": null
      },{
        "title": "Get every nth element from a sequence in Clojure",
        "excerpt":"             Notes   ;; every_nth_element.clj  (def a-list (range 60))  (defn nth-elements [n coll]   (map last (partition n coll)))  (nth-elements 12 a-list)  ","categories": [],
        "tags": [],
        "url": "/2022/12/05/get-every-nth-element-from-a-sequence-in-clojure.html",
        "teaser": null
      },{
        "title": "Refer Clojure Docs Before Coding",
        "excerpt":"             Notes   ;; read_clojure_doc.clj  (def a-list (range 1 61))  (take-nth 5 a-list)  (require '[clojure.set :as cset])  (def spell-numbers {1 \"one\" 2 \"two\" 3 \"three\"})  (cset/map-invert spell-numbers)  ","categories": [],
        "tags": [],
        "url": "/2022/12/18/refer-clojure-docs-before-coding.html",
        "teaser": null
      },{
        "title": "Scanning for Pythagorean triplets in Clojure",
        "excerpt":"Notes 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49...","categories": [],
        "tags": [],
        "url": "/2023/02/15/scanning-for-pythagorean-triplets-in-clojure.html",
        "teaser": null
      },{
        "title": "Histogram from sum of throw of dice pair using Clojure",
        "excerpt":"Notes 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ;; dice_histogram.clj (def faces (range 1 (inc 6))) (def permutations (for [x faces y faces] (list x y))) (defn sum [a b] (+ a b)) (def sum-of-throws (map #(sum (first...","categories": [],
        "tags": [],
        "url": "/2023/02/17/histogram-from-sum-of-throw-of-dice-pair-using-clojure.html",
        "teaser": null
      },{
        "title": "Clojure book beta release 0",
        "excerpt":"Hello All, Clojure bug bit me this weekend, I was frantically adding content to my Clojure book, and I am happy to announce I was able to complete it with a lot of mistakes (which you could spot so that I can correct it). This book is both libre, means...","categories": [],
        "tags": [],
        "url": "/2023/02/26/clojure-book-beta-release-0.html",
        "teaser": null
      },{
        "title": "A simple timer in Clojure",
        "excerpt":"Notes 1 2 3 4 5 6 7 8 9 10 11 12 13 ;; timer.clj (defn print-without-newline [string] (print string) (flush)) (defn timer-for [minutes] (let [seconds (* minutes 60)] (doseq [n (reverse (range seconds))] (Thread/sleep 1000) (print-without-newline (str \"\\r\" (quot n 60) \" m \" (mod n 60) \"...","categories": [],
        "tags": [],
        "url": "/2023/02/27/a-simple-timer-in-clojure.html",
        "teaser": null
      },{
        "title": "New logo for Clojure Diary",
        "excerpt":"I am happy to announce that we now have a logo for Clojure diary. Before we were using the copyrighted Clojure logo. I never knew it was copyrighted, until I did a Clojure biohazard logo, for which some people said that it’s a bad idea since Clojure logo is copyrighted....","categories": [],
        "tags": [],
        "url": "/2023/02/28/new-logo-for-clojure-diary.html",
        "teaser": null
      },{
        "title": "frequencies in Clojure",
        "excerpt":"Notes clj꞉user꞉&gt;  (frequencies [:a :a :b :a :c :b]) {:a 3, :b 2, :c 1} clj꞉user꞉&gt;  (for [x (range 6) y (range 6)] (list x y)) ((0 0) (0 1) (0 2) (0 3) (0 4) (0 5) (1 0) (1 1) (1 2) (1 3) (1 4) (1 5)...","categories": [],
        "tags": [],
        "url": "/2023/03/06/frequencies-in-clojure.html",
        "teaser": null
      },{
        "title": "Digging into deeply nested sequence in Clojure",
        "excerpt":"Notes 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 ;; dig.clj (defn dig [sequence &amp; indexes] (let [first-index (first indexes) rest-indexes (rest indexes) first-indexed-value (get sequence first-index)] (if (empty? rest-indexes) first-indexed-value (recur first-indexed-value rest-indexes)))) (dig [1 [1...","categories": [],
        "tags": [],
        "url": "/2023/03/15/digging-into-deeply-nested-sequence-in-clojure.html",
        "teaser": null
      },{
        "title": "Cover for my Clojure Data Science book",
        "excerpt":"I’m happy to announce that cover for my Clojure Data Science book is ready and this is it. I got the excavator image from Free svg. There is no meaning in this cover, but for those who want meaning for everything, this excavator has no human operator because it operated...","categories": [],
        "tags": [],
        "url": "/2023/03/17/cover-for-my-clojure-data-science-book.html",
        "teaser": null
      },{
        "title": "Strategy pattern in Clojure",
        "excerpt":"Notes clj꞉user꞉&gt;  ; Use `alt+enter` to evaluate ;; strategy pattern (+ 4 3) 7 clj꞉user꞉&gt;  (* 4 3) 12 clj꞉user꞉&gt;  ((if true + *) 4 3) 7 clj꞉user꞉&gt;  ((if false + *) 4 3) 12 clj꞉user꞉&gt;  (defn add [a b] (+ a b)) #'user/add clj꞉user꞉&gt;  (defn multiply [a b ]...","categories": [],
        "tags": [],
        "url": "/2023/03/20/strategy-pattern-in-cojure.html",
        "teaser": null
      },{
        "title": "First edition of my Clojure book is launched",
        "excerpt":"I am happy that first edition of my Clojure book is launched, one can get it here. I thank all those who have helped me to achieve this. Though I have put myself as the author many have contributed to it, for example the AsciiDoctor project, Language Tools, the Free...","categories": [],
        "tags": [],
        "url": "/2023/03/23/first-edition-of-my-clojure-book-is-launched.html",
        "teaser": null
      },{
        "title": "get-in - Probe deeply nested sequences in Clojure",
        "excerpt":"             Notes   clj꞉user꞉&gt;  (get-in [1 [1 2 {:a \"a\" :b \"b\"}] 3] '(1 2 :a)) \"a\" clj꞉user꞉&gt;  (get-in [1 [1 2 {:a \"a\" :b \"b\"}] 3] '(1 2)) {:a \"a\", :b \"b\"}     get-in Clojure docs  ","categories": [],
        "tags": [],
        "url": "/2023/04/04/get-in-probe-deeply-nested-sequences-in-clojure.html",
        "teaser": null
      },{
        "title": "Thread first ans thread last in Clojure",
        "excerpt":"             Notes   1 2 3 4 5 6 7 8 9 ;; thread_first_and_last.clj  ;; thread first (-&gt; 2     (Math/pow 3)) ; 8.0  ;; thread last (-&gt;&gt; 2      (Math/pow 3)) ; 9.0   ","categories": [],
        "tags": [],
        "url": "/2023/04/10/thread-first-ans-thread-last-in-clojure.html",
        "teaser": null
      },{
        "title": "Find out why people live longer using Clojure",
        "excerpt":"Notes 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49...","categories": [],
        "tags": [],
        "url": "/2023/04/18/find-out-why-people-live-longer-using-clojure.html",
        "teaser": null
      },{
        "title": "Using reduce to dig into Clojure sequences",
        "excerpt":"Notes 1 2 3 4 5 6 7 8 ;; digging_deep_into_sequence.clj (def data [1 2 [5 {:a \"a\" :b {:c 42 :d [1 4]}}] 7]) (defn dig [sequence &amp; indexes] (reduce #(get %1 %2) sequence indexes)) (dig data 2 1 :b :c) Writing a dig function in Clojure Clojure get-in...","categories": [],
        "tags": [],
        "url": "/2023/04/26/using-reduce-to-dig-into-clojure-sequences.html",
        "teaser": null
      },{
        "title": "edn files in Clojure",
        "excerpt":"             Notes   1 2 3 4 5 6 7 8 ;; edn_file.clj  (require '[clojure.edn :as edn])  (def spell-numbers   (edn/read-string (slurp \"spell_numbers.edn\")))  (spit \"spell_numbers.edn\" (prn-str (assoc spell-numbers 4 \"Four\")))   spell_numbers.edn   {1 \"One\", 2 \"Two\", 3 \"Three\"}     Learn edn   Clojure edn library  ","categories": [],
        "tags": [],
        "url": "/2023/05/03/edn-files-in-clojure.html",
        "teaser": null
      },{
        "title": "println vs prn-str in Clojure",
        "excerpt":"             Notes   1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 ;; println_vs_prn-str.clj  (def string \"Hello World!\")  (println string)  (type (println string))  (prn-str string)  (type (prn-str string))  (def countries [\"India\" \"Pakistan\" \"Srilanka\" \"Bangladesh\"])  (println countries)  (prn-str countries)   ","categories": [],
        "tags": [],
        "url": "/2023/05/08/println-vs-prn-str-in-clojure.html",
        "teaser": null
      },{
        "title": "Using math and emoji characters in Clojure",
        "excerpt":"Notes 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 ;; math_and_emoji_chars.clj (def α 50) (def β 0.7) (* α β) (def Γ 100) (def happiness-vals {\"😀\" 100 \"😐\" 50 \"😢\" 10}) (happiness-vals...","categories": [],
        "tags": [],
        "url": "/2023/05/17/using-math-and-emoji-characters-in-clojure.html",
        "teaser": null
      },{
        "title": "Starting project in Clojure with leiningen",
        "excerpt":"             Notes      Code  ","categories": [],
        "tags": [],
        "url": "/2023/05/22/starting-project-in-clojure-with-leiningen.html",
        "teaser": null
      },{
        "title": "Creating Static Website With Clojure",
        "excerpt":"             Notes      Code  ","categories": [],
        "tags": [],
        "url": "/2023/05/29/creating-static-website-with-clojure.html",
        "teaser": null
      },{
        "title": "Porting single argument function to accept multiple arguments",
        "excerpt":"             Notes   1 2 3 4 5 6 7 8 9 10 11 ;; porting_single_argument_function_to_accept_multiple_arguments.clj  (defn say-hi   ([name] (str \"Hi \" name))   ([name &amp; many-names] (map say-hi (conj many-names name))))  (say-hi \"Karthik\")  (map say-hi [\"Karthik\" \"Mathew\" \"Orangutaan\"])  (say-hi \"Karthik\" \"Mathew\" \"Orangutaan\")   ","categories": [],
        "tags": [],
        "url": "/2023/06/06/porting-single-argumet-function-to-accept-multiple-arguments.html",
        "teaser": null
      },{
        "title": "Vector distances in Clojure",
        "excerpt":"Notes ;; vector_distance.clj (defn square-of-distance [s1 s2] (Math/pow (- s1 s2) 2)) (defn absolute-distance [s1 s2] (Math/abs (- s1 s2))) (defn vector-distance [v1 v2] (-&gt;&gt; (map square-of-distance v1 v2) (reduce +) Math/sqrt)) (defn manhattan-distance [v1 v2] (-&gt;&gt; (map absolute-distance v1 v2) (reduce +))) (vector-distance [0 0] [3 4]) (manhattan-distance [0...","categories": [],
        "tags": [],
        "url": "/2023/06/12/vector-distances-in-clojure.html",
        "teaser": null
      },{
        "title": "K Nearest Neighbors (KNN) Iris classification using Clojure",
        "excerpt":"             Notes      Plot of iris data   Flower on wikipedia   csv-map   Vector Distances in Clojure   Source Code  ","categories": [],
        "tags": [],
        "url": "/2023/06/22/k-nearest-neighbors-knn-iris-classification-using-clojure.html",
        "teaser": null
      },{
        "title": "Creating a Clojure library for map operations",
        "excerpt":"Notes ;; map_product.clj (defn map-product [m1 m2] (let [m1-keys (keys m1) m2-keys (keys m2) keys (distinct (concat m1-keys m2-keys))] (-&gt;&gt; (map #(assoc {} % (* (get m1 % 0) (get m2 % 0))) keys) (apply merge)))) (defn map-dot-product [m1 m2] (reduce + (vals (map-product m1 m2)))) ;; map_operations.clj (defn scalar-to-map...","categories": [],
        "tags": [],
        "url": "/2023/06/26/creating-a-clojure-library-for-map-operations.html",
        "teaser": null
      },{
        "title": "Finding out magnitude and unit map in Clojure",
        "excerpt":"Notes ;; map_operations.clj (defn scalar-to-map [s map-keys] (reduce #(assoc %1 %2 s) {} map-keys)) (defn map-op [func m1 m2] (if (map? m2) (let [m1-keys (keys m1) m2-keys (keys m2) keys (distinct (concat m1-keys m2-keys))] (-&gt;&gt; (map #(assoc {} % (func (get m1 % 0) (get m2 % 0))) keys) (apply...","categories": [],
        "tags": [],
        "url": "/2023/07/16/finding-out-magnitude-and-unit-map-in-clojure.html",
        "teaser": null
      },{
        "title": "Find out text similarities using Clojure",
        "excerpt":"Notes $ lynx -dump https://en.wikipedia.org/wiki/Cattle &gt; cattle.txt $ lynx -dump https://en.wikipedia.org/wiki/Goat &gt; goat.txt $ lynx -dump https://en.wikipedia.org/wiki/Bolt_\\(fastener\\) &gt; bolt.txt $ lynx -dump https://en.wikipedia.org/wiki/Apple &gt; apple.txt $ lynx -dump https://en.wikipedia.org/wiki/Sewing_machine &gt; sewing_machine.txt Code Finding out magnitude and unit map in Clojure Creating a Clojure library for map operations Find out why...","categories": [],
        "tags": [],
        "url": "/2023/07/17/find-out-text-similarities-using-clojure.html",
        "teaser": null
      },{
        "title": "Clojure Authors! Update your Clojure book(s)",
        "excerpt":"             Notes      Clojure Books   Free programming books   Ascii Doctor   Biff web framework   Kit web framework   I Love Ruby   Brave Search   Jekyll blogging software   yu7 URL shortner  ","categories": [],
        "tags": [],
        "url": "/2023/08/04/clojure-authors-update-your-clojure-book-s.html",
        "teaser": null
      },{
        "title": "You can't convert list of lists to a map in Clojure",
        "excerpt":"Code ;; list_of_list_to_map.clj (def list-of-vectors (list [:a 1] [:b 2])) ; ([:a 1] [:b 2]) (into {} list-of-vectors) ; {:a 1, :b 2} (def list-of-lists (list (list :a 1) (list :b 2))) ; ((:a 1) (:b 2)) (into {} list-of-lists) ; error, why? (into {} (map vec list-of-lists)) ; {:a...","categories": [],
        "tags": [],
        "url": "/2023/09/09/you-can-t-convert-list-of-lists-to-a-map-in-clojure.html",
        "teaser": null
      },{
        "title": "Executing shell commands in Clojure",
        "excerpt":"Code ;; executing_shell_commands.clj (use '[clojure.java.shell :only [sh]]) (require '[clojure.string :as str]) (sh \"cal\") (println (:out (sh \"cal\"))) (def curl_output (:out (sh \"curl\" \"-X\" \"HEAD\" \"-I\" \"https://something.com\"))) (println curl_output) (rest (str/split curl_output #\"\\n\")) (def url \"https://something.com\") (as-&gt; url x (sh \"curl\" \"-X\" \"HEAD\" \"-I\" x) (:out x) (str/split x #\"\\n\") (nth...","categories": [],
        "tags": [],
        "url": "/2023/09/22/executing-shell-commands-in-clojure.html",
        "teaser": null
      },{
        "title": "Converting CURL header to Clojure map",
        "excerpt":"Code ;; curl_headers.clj (use '[clojure.java.shell :only [sh]]) (require '[clojure.string :as str]) ;; Got both below functions from ;; https://stackoverflow.com/questions/31138653/clojure-idiomatic-way-to-write-split-first-and-split-last (defn split-first [s re] (str/split s re 2)) (defn split-last [s re] (let [pattern (re-pattern (str re \"(?!.*\" re \")\"))] (split-first s pattern))) (defn string-to-map [s re] (let [[k v] (map...","categories": [],
        "tags": [],
        "url": "/2023/09/27/converting-curl-header-to-clojure-map.html",
        "teaser": null
      },{
        "title": "Power of Clojure for Clojure newbies and non Clojurists",
        "excerpt":"             Code   ;; clojure_power.clj  (* (+ 1 1) 2)  (defn say-hello [name]   (println (str \"Hello \" name \"!\")))  (say-hello \"Karthik\")  ((if true + *) 3 5)  Notes      Why Lisp?   Why Clojure   Clojure Community   Clojure Books  ","categories": [],
        "tags": [],
        "url": "/2023/10/04/power-of-clojure-for-clojure-newbies-and-non-clojurists.html",
        "teaser": null
      },{
        "title": "Private methods in Clojure",
        "excerpt":"             Notes      Code  ","categories": [],
        "tags": [],
        "url": "/2023/10/05/private-methods-in-clojure.html",
        "teaser": null
      },{
        "title": "Private constants & breaking private functions in Clojure",
        "excerpt":"             Notes      Code   Private methods in Clojure   Reddit discussion that led to this video  ","categories": [],
        "tags": [],
        "url": "/2023/10/08/private-constants-breaking-private-functions-in-clojure.html",
        "teaser": null
      },{
        "title": "Great Clojure Software Programs — 2023",
        "excerpt":"             Notes      Awesome Clojure            https://github.com/razum2um/awesome-clojure           Metabase            https://www.metabase.com/           Logseq            https://logseq.com/           Penpot            https://github.com/penpot/penpot#getting-started           Riemann            http://riemann.io/           Maria            https://www.maria.cloud/gallery?eval=true       https://github.com/mhuebert/maria           XTDB            https://www.xtdb.com/           Puppet            Partially in Clojure       https://github.com/puppetlabs/       https://www.puppet.com/           Babashka            https://babashka.org/           Leiningen            https://leiningen.org/           clj-plot            https://github.com/generateme/cljplot          ","categories": [],
        "tags": [],
        "url": "/2023/10/13/great-clojure-software-programs-2023.html",
        "teaser": null
      },{
        "title": "clj-http and downloading images using Clojure",
        "excerpt":"             Notes      Code   clj-http on Clojars   clj-http on GitHub  ","categories": [],
        "tags": [],
        "url": "/2023/10/16/clj-http-and-downloading-images-using-clojure.html",
        "teaser": null
      },{
        "title": "deknil static website written in Clojure",
        "excerpt":"             Notes      deknil   Code   Bulma   hiccup   markdown-clj  ","categories": [],
        "tags": [],
        "url": "/2023/10/28/deknil-static-website-written-in-clojure.html",
        "teaser": null
      },{
        "title": "for and doseq with conditions",
        "excerpt":"Code ;; for_doseq_with_conditions.clj (for [x [:a :b], y (range 5) :when (odd? y)] [x y]) (doseq [x [:a :b], y (range 5) :when (odd? y)] (prn x y)) (for [x (range 10) :when (even? x), y (range 5) :when (odd? y)] [x y]) (for [x (range 10) :when (odd? x),...","categories": [],
        "tags": [],
        "url": "/2023/10/31/for-and-doseq-with-conditions.html",
        "teaser": null
      },{
        "title": "lead. A blogging system in Clojure, and other Clojure adventures.",
        "excerpt":"             Notes      Lead Code   deknil Telegram Group   clj-style  ","categories": [],
        "tags": [],
        "url": "/2023/11/24/lead-a-blogging-system-in-clojure-and-other-clojure-adventures.html",
        "teaser": null
      },{
        "title": "Only true is true",
        "excerpt":"Code ;; true.clj (true? true) (true? false) (true? 0) (true? 1) (false? 1) (false? false) (false? 0) (number? 1) (= 1 1) (empty? []) (empty? nil) ;; (empty? 1) ; throws exception (nil? nil) (nil? 1) (zero? 0) ;; (zero? nil) ; throws exception ;; (zero? \"278\") ; throws exception...","categories": [],
        "tags": [],
        "url": "/2023/11/29/only-true-is-true.html",
        "teaser": null
      },{
        "title": "Kanipaan 01 - Simple calculator in Clojure",
        "excerpt":"             Notes      Code   Clojure Regular Expression  ","categories": [],
        "tags": [],
        "url": "/2024/01/07/kanipaan-01-simple-calculator-in-clojure.html",
        "teaser": null
      },{
        "title": "Number comparison gotchas in Clojure",
        "excerpt":"             Notes   ;; number_comparison_gotchas.clj  (= 7 7)  (= 7 7.0)  (= (double 7) 7.0)  (= 7.0 7.0)  (= 7 7.0M)  (= 7.0 7.0M)  (= 7.0 (double 7.0M))  (= 7 7/1)  (double 1/3)  ","categories": [],
        "tags": [],
        "url": "/2024/01/20/number-comparison-gotchas-in-clojure.html",
        "teaser": null
      },{
        "title": "Releasing Kanipaan, and lessons learnt",
        "excerpt":"             Notes      Kanipaan   Codeberg   Keep a changelog   Sematic Versioning   GNU Changelog format   Simple CSS   VSCodium  ","categories": [],
        "tags": [],
        "url": "/2024/01/21/releasing-kanipaan-and-lessons-learnt.html",
        "teaser": null
      },{
        "title": "Find first n non-repeating objects in a sequence using Clojure",
        "excerpt":"             Code   ;; first_n_unique_letters.clj  (def string \"rgaraga4agjrj4rikllmrfmghjqwwrwengek\")  (defn sieve [string]   (let [first-4-letters (take 4 string)         unique-letters (set first-4-letters)         length (count unique-letters)]     (if (= length 4)       (clojure.string/join \"\" first-4-letters)       (if (= length 0)         (println \"Nothing found\")         (recur (rest string))))))  Notes      Prime Reacts Video  ","categories": [],
        "tags": [],
        "url": "/2024/02/02/find-first-n-non-repeating-objects-in-a-sequence-using-clojure.html",
        "teaser": null
      },{
        "title": "Necessary variables in let, Tabby A.I suggest with Clojure",
        "excerpt":"Code ;; necessary_variables_in_let.clj (def string \"rgaraga4agjrj4rikllmrfmghjqwwrwengek\") (defn sieve [string] (let [first-4-letters (take 4 string) unique-letters (set first-4-letters) length (count unique-letters)] (if (= length 4) (clojure.string/join \"\" first-4-letters) (if (= length 0) (println \"Nothing found\") (recur (rest string)))))) (defn sieve-2 [string] (let [first-4-letters (take 4 string) length (-&gt;&gt; first-4-letters set count)]...","categories": [],
        "tags": [],
        "url": "/2024/03/02/necessary-variables-in-let-tabby-a-i-suggest-with-clojure.html",
        "teaser": null
      },{
        "title": "Better Sieve - Finding first n unique characters in Clojure",
        "excerpt":"             Code   ;; better_sieve.clj  (def string \"rgaraga4agjrj4rikllmrfmghjqwwrwengek\")  (defn sieve [length coll]   (-&gt;&gt; coll        (partition length 1)        (some #(when (apply distinct? %)                 (apply str %)))))  (sieve 4 string)  Notes      Comment by  Cameron Desautels   Cameron Desautels on Youtube  ","categories": [],
        "tags": [],
        "url": "/2024/03/12/better-sieve-finding-first-n-unique-characters-in-clojure.html",
        "teaser": null
      },{
        "title": "Fixing cider-nrepl warning in Calva",
        "excerpt":"             Notes   In ~/.lein/profiles.clj add:   {:repl {:plugins [[cider/cider-nrepl \"0.47.0\"]]}}  ","categories": [],
        "tags": [],
        "url": "/2024/04/06/fixing-cider-nrepl-warning-in-calva.html",
        "teaser": null
      },{
        "title": "Proper way to compare numbers in Clojure",
        "excerpt":"Code ;; number_comparison_right_way.clj (= 7 7) (= 7 7.0) (type 7) (type 7.0) (= 7 7N) (type 7N) (= 7.0 7N) (type 7M) (= 7.0 7M) (= 7 7M) (= (double 7) 7.0) (== 7 7.0) (== 7 7.0M) (== 7.0 7M) (== Math/PI (/ 22 7 )) (== 7N...","categories": [],
        "tags": [],
        "url": "/2024/05/22/proper-way-to-compare-numbers-in-clojure.html",
        "teaser": null
      },{
        "title": " Stats With Clojure Book, Injee, and Clojure Diary updates",
        "excerpt":"             Correction   It should be noted that Practical.li is not run by Daniel Slutsky.   Notes      Stats With Clojure Book   Injee   Learn Clojure Videos  ","categories": [],
        "tags": [],
        "url": "/2024/06/29/stats-with-clojure-book-injee-and-clojure-diary-updates.html",
        "teaser": null
      },{
        "title": "for when",
        "excerpt":"Code ;; for_when.clj (for [x [:a :b] y (range 5) :when (odd? y)] [x y]) (for [x [:a :b] y '(1 3)] [x y]) (let [odd-y (filter odd? (range 5))] (for [x [:a :b] y odd-y] [x y])) (for [x [:a :b :c] y '(1 3)] [x y]) (for [x...","categories": [],
        "tags": [],
        "url": "/2024/07/01/for-when.html",
        "teaser": null
      },{
        "title": "Packaging Clojure app for Mac",
        "excerpt":"Code $ lein ring uberjar $ jpackage --name injee --icon /Users/karthik/Downloads/injee_perfect_square.icns --input ./ --main-jar injee-0.2.0.jar And I found it in my terminal’s history, this is how you try to create native executable when you GraalVM JDK on your system: $ native-image -jar injee-ring-0.2.0-standalone.jar And it didn’t work out for me...","categories": [],
        "tags": [],
        "url": "/2024/08/01/packaging-clojure-app-for-mac.html",
        "teaser": null
      },{
        "title": "Greatest of three numbers in Clojure",
        "excerpt":"Code ;; greatest_of_three_numbers.clj (def a 27) (def b 20) (def c 25) (if (&gt; a b) (if (&gt; a c) a c) (if (&gt; b c) b c)) (if (&gt; a b) (if (&gt; a c) a c) (if (&gt; b c) b c)) (if (and (&gt; a b) (&gt;...","categories": [],
        "tags": [],
        "url": "/2024/08/16/greatest-of-three-numbers-in-clojure.html",
        "teaser": null
      },{
        "title": "Shipping Jar with Shell",
        "excerpt":"             Notes      Injee run   Converting jar to dmg using jpackage  ","categories": [],
        "tags": [],
        "url": "/2024/08/20/shipping-jar-with-shell.html",
        "teaser": null
      },{
        "title": "Generating multiplication tables with Clojure",
        "excerpt":"Code (defn single-line [number multiplicant] (str number \" X \" multiplicant \" = \" (* number multiplicant))) ;; (def num 5) ;; (println ;; (clojure.string/join \"\\n\" ;; (map single-line (repeat 10 5) (range 1 11)))) (defn single-table [number] (clojure.string/join \"\\n\" (map single-line (repeat 10 number) (range 1 11)))) (println (clojure.string/join...","categories": [],
        "tags": [],
        "url": "/2024/09/29/generating-multiplication-tables-with-clojure.html",
        "teaser": null
      },{
        "title": "Pagination in Clojure is so easy",
        "excerpt":"             Code   (defn paginate [coll page per-page]   (nth (partition-all per-page coll) (- page 1) []))  ;; usage (paginate (range 1 15) 2 4)  Notes      Injee  ","categories": [],
        "tags": [],
        "url": "/2024/10/11/pagination-in-clojure-is-so-easy.html",
        "teaser": null
      },{
        "title": "3 coin flip Monte Carlo simulation in Clojure",
        "excerpt":"Code ;; three_coin_toss_montecarlo.clj ;; Montecarlo simulation of 3 coin toss (defn toss-thrice [] (repeatedly 3 #(rand-nth [\"H\" \"T\"]))) (defn samples [num] (repeatedly num toss-thrice)) (defn nil-to-zero [x] (if (nil? x) 0 x)) (defn side-count [coll side] (nil-to-zero (get (frequencies coll) side))) (defn heads-count [coll] (side-count coll \"H\")) (defn tails-count [coll]...","categories": [],
        "tags": [],
        "url": "/2024/10/18/3-coin-flip-monte-carlo-simulation-in-clojure.html",
        "teaser": null
      },{
        "title": "Project Euler Problem 8",
        "excerpt":"Code (def data \"73167176531330624919225119674426574742355349194934 96983520312774506326239578318016984801869478851843 85861560789112949495459501737958331952853208805511 12540698747158523863050715693290963295227443043557 66896648950445244523161731856403098711121722383113 62229893423380308135336276614282806444486645238749 30358907296290491560440772390713810515859307960866 70172427121883998797908792274921901699720888093776 65727333001053367881220235421809751254540594752243 52584907711670556013604839586446706324415722155397 53697817977846174064955149290862569321978468622482 83972241375657056057490261407972968652414535100474 82166370484403199890008895243450658541227588666881 16427171479924442928230863465674813919123162824586 17866458359124566529476545682848912883142607690042 24219022671055626321111109370544217506941658960408 07198403850962455444362981230987879927244284909188 84580156166097919133875499200524063689912560717606 05886116467109405077541002256983155200055935729725 71636269561882670428252483600823257530420752963450\") (def num-str (clojure.string/replace data #\"\\n\" \"\")) (def digit-chars (partition 13 1 num-str)) (defn product [chars] (reduce * (map #(read-string (str %)) chars))) (defn product-and-chars [chars] [(product chars)...","categories": [],
        "tags": [],
        "url": "/2024/11/11/project-euler-problem-8.html",
        "teaser": null
      },{
        "title": "Project Euler Problem 9",
        "excerpt":"Code ;; euler_9.clj (def possibilities (for [a (range 1 1001) b (range (+ a 1) 1001)] [a b])) (defn satisfies-condition [possibility] (let [a (first possibility) b (second possibility) c (- 1000 (+ a b))] (== (+ (* a a) (* b b)) (* c c)))) ;; (filter satisfies-condition possibilities) (let...","categories": [],
        "tags": [],
        "url": "/2024/11/13/project-euler-problem-9.html",
        "teaser": null
      },{
        "title": "Clojure is really nice to write",
        "excerpt":"I was a happy Ruby on Rails programmer in my previous office, and out of the blue, my boss asked me to learn ReactJS. JavaScript doesn’t suck for me, it sucks, it sucks, sucks, sucks, and it was no way I am going to learn it. But my boss persisted,...","categories": [],
        "tags": [],
        "url": "/2024/12/01/clojure-is-really-nice-to-write.html",
        "teaser": null
      },{
        "title": "Testing ring handlers while using non default port",
        "excerpt":"             Notes      Commit   Injee  ","categories": [],
        "tags": [],
        "url": "/2024/12/06/testing-ring-handlers-while-using-non-default-port.html",
        "teaser": null
      },{
        "title": "What is form in Clojure?",
        "excerpt":"In Clojure, a form is a piece of code that can be evaluated. Forms can be simple expressions, such as numbers or symbols, or they can be more complex structures, such as lists or maps. For example, the following are all forms in Clojure: 42 (a number) \"hello\" (a string)...","categories": [],
        "tags": [],
        "url": "/2025/01/02/what-is-form-in-clojure.html",
        "teaser": null
      },{
        "title": "Printing Color in Terminal with Clojure",
        "excerpt":"In Clojure, printing colored text to the terminal can be achieved by using ANSI escape codes, which are sequences of characters that modify text attributes like color, boldness, or background color. These codes are recognized by most modern terminal emulators. Here’s how you can print colored text in Clojure using...","categories": [],
        "tags": [],
        "url": "/2025/01/08/printing-in-color.html",
        "teaser": null
      },{
        "title": "Where to store your (image) files in Leiningen project, and how to fetch them?",
        "excerpt":"Notes Create new app using: $ lein new app image_in_resources Place clojure_diary-logo.png in resources/images/ folder. project.clj content: (defproject image_in_resources \"0.1.0-SNAPSHOT\" :description \"FIXME: write description\" :url \"http://example.com/FIXME\" :license {:name \"EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0\" :url \"https://www.eclipse.org/legal/epl-2.0/\"} :dependencies [[org.clojure/clojure \"1.11.1\"]] :main ^:skip-aot image-in-resources.core :target-path \"target/%s\" :profiles {:uberjar {:aot :all :jvm-opts [\"-Dclojure.compiler.direct-linking=true\"]}}) src/image_in_resources/core.clj...","categories": [],
        "tags": [],
        "url": "/2025/01/17/where-to-store-your-image-files-in-leiningen-project-and-how-to-fetch-them.html",
        "teaser": null
      },{
        "title": "Extending Java Class in Clojure",
        "excerpt":"Code ;; extend_java_class.clj (defprotocol Wishy (wish [this])) (extend-type String Wishy (wish [this] (str \"Hello \" this \"!\"))) (wish \"Karthik\") (wish 1) ; puts out an error (extend-type java.lang.Long Wishy (wish [this] (str this \" is a great number!\"))) (wish 1) (defprotocol Greeting (greet [this]) (hello [this])) (extend-type String Greeting (greet...","categories": [],
        "tags": [],
        "url": "/2025/02/26/extending-java-class-in-clojure.html",
        "teaser": null
      },{
        "title": "Cloverage — Clojure Test Coverage",
        "excerpt":"             Notes      Cloverage is a Clojure tool that provides test coverage statistics for your Clojure code.   Injee, is the t project in which I am using Cloverage.   ","categories": [],
        "tags": [],
        "url": "/2025/03/02/cloverage-clojure-test-coverage.html",
        "teaser": null
      },{
        "title": "Filtering maps based on key value pairs in Clojure",
        "excerpt":"Code (defn key-has-value? [key value map] (= value (get map key))) (defn map-filter [key value seq-of-maps] (filter #(key-has-value? key value %) seq-of-maps)) (defn maps-having [filter-map seq-of-map] (if (empty? filter-map) seq-of-map (let [[key value] (first filter-map) filtered-seq (map-filter key value seq-of-map)] (recur (rest filter-map) filtered-seq)))) ;; let's test the above code...","categories": [],
        "tags": [],
        "url": "/2025/03/04/filtering-maps-based-on-key-value-pairs-in-clojure.html",
        "teaser": null
      }]
